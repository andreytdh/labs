package lesson11;

public class Test {

    public static void main(String[] args) {
        Address home = new Address("Russia", "Spb", "Koroleva", 0, 0);

        Human dother= new Human("Vasileva", "Aleksandra", "Andreevna", 2020, home);
        Human me = new Human("Vasilev", "Andrei", "Ivanovich", 1988, home);
        Human wife = new Human("Smetannikova", "Aleksandra", "Danilovna", 1995, home);
        me.setPartner(wife);
        wife.setPartner(me);
        me.setChild1(dother);
        wife.setChild1(dother);
    }
}
