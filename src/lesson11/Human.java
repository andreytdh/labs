package lesson11;

public class Human {
    private String surName;
    private String name;
    private String middleName;
    private int year;
    private Address address;
    private Human partner;
    private Human child1;
    private Human child2;
    private Human child3;

    public Human(String surName, String name, int year) {
        this.surName = surName;
        this.name = name;
        if (year > 1880) {
            this.year = year;
        }
    }

    public Human(String surName, String name, String middleName, int year) {
        this(surName, name, year);
        this.middleName = middleName;
    }

    public Human(String surName, String name, int year, Address address) {
        this(surName, name, year);
        this.address = address;
    }

    public Human(String surName, String name, String middleName, int year, Address address) {
        this(surName, name, middleName, year);
        this.address = address;
    }

    public Human getPartner() {
        return partner;
    }

    public void setPartner(Human partner) {
        this.partner = partner;
    }

    public Human getChild1() {
        return child1;
    }

    public void setChild1(Human child1) {
        this.child1 = child1;
    }

    public Human getChild2() {
        return child2;
    }

    public void setChild2(Human child2) {
        this.child2 = child2;
    }

    public Human getChild3() {
        return child3;
    }

    public void setChild3(Human child3) {
        this.child3 = child3;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}