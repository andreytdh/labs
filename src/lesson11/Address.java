package lesson11;

public class Address {
    private String country;
    private String city;
    private String street;
    private int housenumber;
    private int appartment;
    private int index;

    public Address(String country, String city, String street, int housenumber, int appartment, int index) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.housenumber = housenumber;
        this.appartment = appartment;
        this.index = index;
    }

    public Address(String country, String city, String street, int housenumber, int appartment) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.housenumber = housenumber;
        this.appartment = appartment;
        index = 100000;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(int housenumber) {
        this.housenumber = housenumber;
    }

    public int getAppartment() {
        return appartment;
    }

    public void setAppartment(int appartment) {
        this.appartment = appartment;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static void main(String[] args) {
        Address test = new Address("Russia", "Spb", "Koroleva", 0, 0);
        System.out.println(test.index);
    }
}
