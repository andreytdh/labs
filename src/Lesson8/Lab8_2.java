package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_2 {

    public static String makeWrapWord(String word, String wrap) {
        return wrap.substring(0,2) + word + wrap.substring(2,4);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a word: ");
        String word = br.readLine();
        System.out.print("Enter a wrap: ");
        String wrap = br.readLine();
        if (wrap.length()>=4) {
        System.out.println(makeWrapWord(word, wrap));
        }
        else System.out.println("Enter a wrap longer then 4 symbols");
    }
}

