package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_6 {

    public static boolean oddLength(double d){
        String text = String.valueOf(d);
        if (text.length()%2 !=0) {
            return true;
        }
        else return false;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a digit: ");
        double digit = Double.parseDouble(br.readLine());
        System.out.println(oddLength(digit));
    }
}
