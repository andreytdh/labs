package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_3 {
    public static String half(String word) {
        return word.substring(0, word.length()/2);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a word: ");
        String word = br.readLine();

        System.out.println(half(word));
    }
}
