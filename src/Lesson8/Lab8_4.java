package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_4 {
    final static String checkWord = "odd";
    public static boolean endsOod(String word) {
        if (word.length()<3) {
            return false;
        }
        else if (word.equals(checkWord)) {return true;}
        else if (word.substring(word.length()-3).equals(checkWord)) {
         // if (word.endsWith("odd")) {
            return true;
        }
        else return false;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a word: ");
        String word = br.readLine();

        System.out.println(endsOod(word));
    }
}

