package Lesson8;

//Верните количество вхождений искомой строки в строке для поиска (игнорируя регистр)
//stringCounter("Java programmers love Java", "java") -> 2
//public static int stringCounter(String s, String searchString){

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_7 {
    static int counter = 0;
    static int index = 0;
    public static int stringCounter(String s, String searchString){
            while ((index = s.toLowerCase().indexOf(searchString, index))!=-1) {
                counter++;
                index++;
        }
        return counter;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a word: ");
        String word = br.readLine();
        System.out.print("Enter a searchWord: ");
        String searchWord = br.readLine();

        System.out.println(stringCounter(word, searchWord));
    }

}
