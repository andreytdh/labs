package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Напишите вежливую функцию, которая возвращает следующую строку:
//"Hello Alice !"
public class Lab8_1 {
    public static String politeMethod(String name) {
    return "Hello " + name + " !";
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter your name: ");
        String name = br.readLine();

        System.out.println(politeMethod(name));
    }
}
