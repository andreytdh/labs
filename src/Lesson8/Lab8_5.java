package Lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab8_5 {
    static int counter = 0;

    public static int charCounter(String s, char c) {
          char[] string = s.toCharArray();

        for (int i = 0; i < string.length; i++) {
            if (string[i] == c) {
                counter++;
            }
        }
        return counter;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a word: ");
        String word = br.readLine();

        System.out.print("Enter a char: ");
        char a = br.readLine().charAt(0);

        System.out.println(charCounter(word, a));
    }
}
