package Lesson19;

public class Package {
        int volume;
        Object[] packet;

        public Package(int volume) {
            this.volume = volume;
            this.packet = new Object[volume];
        }

        public boolean put(Object object) {
            int freeindex = 0;
            for (int i = 0; i < packet.length ; i++) {
                if (packet[i] == null) {
                    freeindex = i;
                }
                else freeindex = packet.length;
            }
            try {
                packet[freeindex] = object;
                return true;
            }
            catch (ArrayIndexOutOfBoundsException e) {
                //e.printStackTrace();
                return false;
            }
        }

    }

