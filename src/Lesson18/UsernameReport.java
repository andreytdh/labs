package Lesson18;

public class UsernameReport extends Report {
    int number;

    public UsernameReport(String name) {
        super(name);
        this.number = Report.count;
    }

    @Override
    public String getReport() {
        return this.number + ": " + System.getProperty("user.name") + " " + this.name;
    }
}
