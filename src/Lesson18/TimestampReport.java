package Lesson18;

import java.util.Date;

public class TimestampReport extends Report {
    Date currentDate;
    int number;

    public TimestampReport(String name) {
        super(name);
        this.number = Report.count;
    }


    @Override
    public String getReport() {
        currentDate = new Date(System.currentTimeMillis());
        return this.number + ": " + currentDate + " " + this.name;
    }
}
