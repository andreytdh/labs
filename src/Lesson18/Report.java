package Lesson18;

public class Report {
    static int count = 0;
    String name;
    int number;

    public Report(String name) {
        this.name = name;
        Report.count++;
        this.number = Report.count;
    }

    public String getReport() {
        return (this.number + ": " + this.name);
    }
}
