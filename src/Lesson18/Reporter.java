package Lesson18;

public class Reporter {

    public void report(Report report) {
        System.out.println("My current report: " + report.getReport());
    }

    public static void main(String[] args) {
        Report report1 = new Report("Test1");
        Report report2 = new TimestampReport("Test2");
        Report report3 = new UsernameReport("Test3");

        Reporter reporter = new Reporter();

        reporter.report(report1);
        reporter.report(report2);
        reporter.report(report3);


    }

}
