package Lesson22;

public class StackOverFlowEr {

    public static void print(int x) {
        if (x==0) return;
        else print(++x);
    }

    public static void main(String[] args) {
        print(1);
    }
}
