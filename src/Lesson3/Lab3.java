package Lesson3;

public class Lab3 {
    public static void main(String[] args) {
        byte a = 12;
        short b = 30000;
        int c = 25000000;
        long d = 890000000;
        double e = 3.2;
        char f = 32;
        boolean t = true;
        String text = "test";

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(t);
        System.out.println(text);
    }
}
