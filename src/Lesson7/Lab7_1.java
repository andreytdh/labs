package Lesson7;

// Необходимо вывести число фибоначчи двумя спобами, рекурсивно и в цикле. По желанию вывести время затраченно на каждый способ.
// Испльзуйте long currentTime = System.nanoTime(); для узнавания текущего времени в наносекундах

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab7_1 {



    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(br.readLine());
        long currentTime = System.nanoTime();
            int a = 0;
            int b = 1;
        for (int i = 0; i < x ; i++) {
            int next = a+b;
            System.out.print(a + " ");
            a = b;
            b = next;
        }
    long endTime = System.nanoTime();
        System.out.println( "\n" +"loop for fibonachi took " + (double)(endTime-currentTime)/1000000 + " ms");

    }
}
