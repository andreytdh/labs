package Lesson7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab7_2 {
    final static int ROCK = 0;
    final static int PAPER = 1;
    final static int SCISSOR = 2;
    final static int LIZARD = 3;
    final static int SPOCK = 4;

    final static String[] values = {"ROCK", "PAPER", "SCISSOR", "LIZARD", "SPOCK"};

    public static void printKeys(int x, int y) {
        System.out.println("user has chosen: "+ values[x] + ", computer has chosen: " + values[y]);
    }

    public static void whoWins(int x, int y) {
        if (x == y) {
            System.out.println("It's a draw");
        }

        else if (x == 0) {
            if (y == 1 || y == 4) {
                System.out.println("Computer wins");
            }
            else if (y == 2 || y == 3) {
                System.out.println("User wins");
            }
        }

        else if (x == 1) {
            if (y == 0 || y == 4) {
                System.out.println("User wins");
            }
            else if (y == 2 || y == 3) {
                System.out.println("Computer wins");
            }
        }

        if (x == 2) {
            if (y == 0 || y == 4 ) {
                System.out.println("Computer wins");
            }
            else if (y == 1 || y == 3) {
                System.out.println("User wins");
            }
        }

        if (x == 3) {
            if (y == 0 || y == 2) {
                System.out.println("Computer wins");
            }
            else if (y == 1 || y == 4) {
                System.out.println("User wins");
            }
        }

        if (x == 4) {
            if (y == 0 || y == 2) {
                System.out.println("User wins");
            }
            else if (y == 1 || y == 3) {
                System.out.println("Computer wins");
            }
        }
    }


    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean gameIsInProgress = true;

                while (gameIsInProgress) {
                    System.out.print("Please enter your choise: ");
                    int userChoise = Integer.parseInt(br.readLine());
                    if (userChoise == 9) {
                        gameIsInProgress = false;
                        System.out.println("Game is over");
                    }
                    else if (userChoise<=4 & userChoise>=0) {
                        int compChoise = (int) Math.round(Math.random() * 4);
                        printKeys(userChoise, compChoise);
                        whoWins(userChoise, compChoise);
                    }
                    else System.out.println("Please enter correct choise and follow the rules");
                }
    }
}
