package Lesson4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab4_2 {
    public static void main(String[] args)  throws IOException {
        final int digit = 100;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double digit2 = Double.parseDouble(br.readLine());

        if (digit2!=0) {
            System.out.println(digit/digit2);
        }
        else System.out.println(Double.MAX_VALUE);
    }
}
