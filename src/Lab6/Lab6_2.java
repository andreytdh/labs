package Lab6;

// Объявить и проинициализировать массив, содержащий положительные и отрицательные целые числа,
// вычислить сумму только четных положительных элементов

public class Lab6_2 {

    static int [] array = {20, -1, 0 , 10, -20, -2, 3, 7 , 10, 25 , -8, -3, 12, -6};
    static int sum = 0;

    public static int findSum(int[] x) {
        for (int i = 0; i < x.length ; i+=2) {
            if (i%2 == 0 && x[i] > 0) {
                sum += x[i];
            }
        }
        return sum;
    }


    public static void main(String[] args) {
        System.out.println(findSum(array));
    }
}
