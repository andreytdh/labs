package Lab6;

//Выполнить реверсирование массива (первый элемент становится последним,
//последний - первым и т.д). Вывести элементы первоначального массива и после выполнения реверсирования.

public class Lab6_1 {

    static int[] array = new int[10];


    public static int[] reverseArray(int[] x) {
        int[] reversearray = new int[x.length];
        for (int i = 0; i < x.length; i++) {
            reversearray[reversearray.length-1-i] = array[i];
        }
        return reversearray;
    }

    public static void main(String[] args) {

        for (int i = 0; i <array.length ; i++) {
            array[i] = i + 1;
        }

        for (int element: array) {
            System.out.print(element + " ");
        }
        System.out.print("\n");

        for (int element: reverseArray(array)) {
            System.out.print(element + " ");
        }

    }

}
