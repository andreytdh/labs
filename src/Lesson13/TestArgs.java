package Lesson13;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestArgs {
    public static void main(String[] args) throws IOException {
        String action = args[0];
        String fileInputName = args[1];
        String fileOutputName = args[2];

        switch (action) {
            case "-e": {
                FileInputStream input = new FileInputStream(fileInputName);
                FileOutputStream output = new FileOutputStream(fileOutputName);
                byte[] buffer = new byte[input.available()];
                byte[] code = new byte[buffer.length];
                while (input.available() > 0) {
                    input.read(buffer);
                    for (int i = 0; i < buffer.length; i++) {
                        code[i] = (byte) (buffer[i] + 1);
                    }
                    output.write(code);
                }
                input.close();
                output.close();
                break;
            }

            case "-d": {
                FileInputStream input = new FileInputStream(fileInputName);
                FileOutputStream output = new FileOutputStream(fileOutputName);
                byte[] buffer = new byte[input.available()];
                byte[] decode = new byte[buffer.length];
                while (input.available() > 0) {
                    input.read(buffer);
                    for (int i = 0; i < buffer.length; i++) {
                        decode[i] = (byte) (buffer[i] - 1);
                    }
                    output.write(decode);
                }
                input.close();
                output.close();
                break;
            }
        }
    }
}

