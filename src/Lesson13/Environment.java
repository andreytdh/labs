package Lesson13;
import stub.Simulator;
import test.*;

public class Environment {
    public static void main(String[] args) {

        System.out.println(test.TestCase.startTestCase());
        System.out.println(test.TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());
    }
}
