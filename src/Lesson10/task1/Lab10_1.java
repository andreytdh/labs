package Lesson10.task1;

public class Lab10_1 {
    public static void main(String[] args) {

        //test Circle
        Circle krug = new Circle(5);
        Circle.getPerimeter(krug);
        Circle.getArea(krug);
        krug.moveTo(1,2);

        //test Restangle
        Restangle pryamougolnik = new Restangle(4, 8);
        Restangle.getPerimeter(pryamougolnik);
        Restangle.getArea(pryamougolnik);
        pryamougolnik.moveTo(8, 1);

        //test Square
        Square kvadrat = new Square(5);
        Square.getPerimeter(kvadrat);
        Square.getArea(kvadrat);
        kvadrat.moveTo(0, 2);
    }
}
