package Lesson10.task1;

public interface Movable {
    public void moveTo(int x, int y);
}
