package Lesson10.task1;

public class Circle implements Movable {

    private int radius;
    private Point center;

    public Circle(int radius) {
        this.radius = radius;
        this.center = new Point(0,0);
        System.out.println("Circle with center (" + this.center.getX() + "; " + this.center.getY() + ") was created");
    }

    public static void getPerimeter(Circle circle) {
        System.out.println("Perimeter is: " + (double) circle.radius*2*Math.PI);
    }

    public static void getArea(Circle circle) {
        System.out.println("Area is: " + (double) circle.radius*circle.radius*Math.PI);
    }


    @Override
    public void moveTo(int x, int y) {
        System.out.println("Current position is: x = "+ this.center.getX() + "; y = " + this.center.getY());
        this.center.setX(x);
        this.center.setY(y);
        System.out.println("Position changed to: x = "+ this.center.getX() + "; y = " + this.center.getY());
    }
}
