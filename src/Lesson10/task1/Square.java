package Lesson10.task1;

public class Square implements Movable {

    private int height;
    private int widht;
    private Point topleftcorner;

    public Square(int height) {
        this.height = height;
        this.widht = height;
        this.topleftcorner = new Point(0, 0);
        System.out.println("Square with topleftcorner (" + this.topleftcorner.getX() + "; " + this.topleftcorner.getY() + ") was created");
    }

    public static void getPerimeter(Square square) {
        System.out.println("Perimeter is: " + square.height*4);
    }

    public static void getArea(Square square) {
        System.out.println("Area is: " + square.height*square.height);
    }

    @Override
    public void moveTo(int x, int y) {
        System.out.println("Current position is: x = "+ this.topleftcorner.getX() + "; y = " + this.topleftcorner.getY());
        this.topleftcorner.setX(x);
        this.topleftcorner.setY(y);
        System.out.println("Position changed to: x = "+ this.topleftcorner.getX() + "; y = " + this.topleftcorner.getY());
    }
}
