package Lesson10.task1;

public class Restangle implements Movable {

    private int height;
    private int width;
    Point topleftcorner;


    public Restangle(int height, int width) {
        this.height = height;
        this.width = width;
        this.topleftcorner = new Point(0, 0);
        System.out.println("Restangle with topleftcorner (" + this.topleftcorner.getX() + "; " + this.topleftcorner.getY() + ") was created");
    }

    public static void getPerimeter(Restangle restangle) {
        System.out.println("Perimeter is: " +  (restangle.height*2 + restangle.width*2));
    }

    public static void getArea(Restangle restangle) {
        System.out.println("Area is: " +  restangle.width*restangle.height);
    }

    @Override
    public void moveTo(int x, int y) {
        System.out.println("Current position is: x = "+ this.topleftcorner.getX() + "; y = " + this.topleftcorner.getY());
        this.topleftcorner.setX(x);
        this.topleftcorner.setY(y);
        System.out.println("Position changed to: x = "+ this.topleftcorner.getX() + "; y = " + this.topleftcorner.getY());
    }
}
