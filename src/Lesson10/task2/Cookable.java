package Lesson10.task2;

public interface Cookable {
    public void cook() throws InterruptedException;
}
