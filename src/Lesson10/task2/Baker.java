package Lesson10.task2;

public class Baker {
    String name;

    public Baker(String name) {
        this.name = name;
    }

    public static void main(String[] args) throws InterruptedException {
        Baker me = new Baker("Andrei");

        Cooker elektro = new Cooker(true);

        elektro.cook();
    }
}
