package Lesson10.task2;

public  class Cooker implements Cookable {
    private int temperature;
    private boolean isEnabled;
    private boolean isLoaded;
    private boolean isElektro;

    public Cooker(boolean isElektro) {
        this.temperature = 0;
        this.isEnabled = false;
        this.isLoaded = false;
        this.isElektro = isElektro;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        if (temperature < 0) {
            System.out.println("Operation is not supported. It's not a fridge!");
        }
        else if (temperature == 0) {
            this.temperature = temperature;
            this.isEnabled = false;
        }
        else if (temperature > 0 && temperature <=220) {
            this.temperature = temperature;
            this.isEnabled = true;
        }
        else if (temperature > 220) {
            System.out.println("Operation is not supported.");
        }
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        if (enabled = false) {
            this.setTemperature(0);
        }
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }

    @Override
    public void cook() throws InterruptedException {
        setLoaded(true);
        setEnabled(true);
        setTemperature(180);
        System.out.println("Please wait for your dish");
        Thread.sleep(2000);
        System.out.println("Your dish is ready. Take it off");
        setEnabled(false);
    }
}
