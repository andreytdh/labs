package Lesson16.Jars;
import Lesson16.Products.Product;


import java.util.ArrayList;

public class Jar {

    private String name;
    private ArrayList<Product> content;

    public Jar(String name) {
        this.name = name;
        this.content = new ArrayList<Product>();
    }

    public Jar() {
    }

    public Product take() {
        Product eat = this.content.get(0);
    return eat;
    }

    public void addProduct(Product product) {
        this.content.add(product);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Product> getContent() {
        return content;
    }
}
