package Lesson16.Test;

import Lesson16.Jars.Jar;
import Lesson16.Cupboards.Сupboard;
import Lesson16.Products.GiftsOfTheForest.Berries;
import Lesson16.Products.GiftsOfTheForest.Mushrooms;
import Lesson16.Products.Product;
import Lesson16.Products.Vegetables.Cucumber;
import Lesson16.Products.Vegetables.Tomato;

public class User {
    public static final String name = "Пользователь";

    public static void eat(Product product) {
        System.out.println(User.name + " cъел " + product.getName() + ". Это были " + product.getName() +", вкусно!");
    }

    public static void main(String[] args) {

        Product eat1 = new Berries();
        Product eat2 = new Mushrooms();
        Product eat3 = new Cucumber();
        Product eat4 = new Tomato();


        Сupboard cb1 = new Сupboard(5, "шкаф1");
        Jar jar1 = new Jar("банка1");
        Jar jar2 = new Jar("банка2");
        Jar jar3 = new Jar("банка3");
        Jar jar4 = new Jar("банка4");
//        Jar jar5 = new Jar("банка5");

        jar1.addProduct(eat1);
        jar2.addProduct(eat3);
        cb1.putJar(jar1);
        cb1.putJar(jar2);
        jar3.addProduct(eat1);
        jar3.addProduct(eat2);
        jar4.addProduct(eat4);
        cb1.putJar(jar4);
        cb1.putJar(jar3);
        //cb1.getJars();

       User.eat(cb1.takeJar().take());

    }

    }
