package Lesson16.Cupboards;

import Lesson16.Jars.Jar;

import java.util.ArrayList;

public class Сupboard {
    private int size;
    private String name;
    private ArrayList<Jar> jars;

    public Сupboard(int size, String name) {
        this.jars = new ArrayList<Jar>();
        this.name = name;
        this.size = size;
        }

    public void putJar(Jar jar) {
        this.jars.add(jar);
    }


    public Jar takeJar() {
        Jar backup = new Jar();
        if (this.jars.size()>0) {
            backup = this.jars.get(0);
        }
        this.jars.remove(0);
        return backup;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public void getJars() {
        for (int i = 0; i < jars.size(); i++) {
            System.out.println(jars.get(i));
        }
    }
}
