package Lesson21;

public class Trumpet implements Playable{
    final static String name = "трубы";

    int volume;

    public Trumpet(int volume) {
        this.volume = volume;
    }


    @Override
    public void play() {
        System.out.println("Играют " + Trumpet.name + " c объемом в " + this.volume + " см3");
    }
}
