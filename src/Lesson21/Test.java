package Lesson21;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {

        ArrayList<Playable> list = new ArrayList<>();
        list.add(new Guitar(7));
        list.add(new Drum(50));
        list.add(new Trumpet(200));

        for (Playable entry : list) {
            entry.play();
        }
    }
}
