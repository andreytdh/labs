package Lesson21;

public interface Playable {
 String KEY = "До мажор";

    void play();

    default void defaultplay() {
        System.out.println(Playable.KEY);
    }
}
