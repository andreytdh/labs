package Lesson21;

public class Guitar implements Playable{
    final static String name = "гитара";
    int count;


    public Guitar(int count) {
        this.count = count;
    }

    @Override
    public void play() {
        if (count ==1) {
            System.out.println("Играет " + Guitar.name + " c " + this.count + " струной.");
        }
        else System.out.println("Играет " + Guitar.name + " c " + this.count + " струнами.");
    }
}
