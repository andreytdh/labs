package Lesson21;

public class Drum implements Playable{
    final static String name = "барабан";

    int size;

    public Drum(int size) {
        this.size = size;
    }


    @Override
    public void play() {
        System.out.println("Играет " + Drum.name + " размером в " + this.size + " см.");
    }
}
