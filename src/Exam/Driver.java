package Exam;

import java.util.ArrayList;

public class Driver implements Carsharable{
    public enum Driverlicense {
        A,
        AB
    }
    private Driverlicense license;

    public Driver(Driverlicense license) {
        this.license = license;
    }

    public Driverlicense getLicense() {
        return license;
    }

    public void getavailable(ArrayList<Vehicle> list) {
        for (Vehicle entry : list) {
            if (entry.isFree()) {
                if (getLicense() == Driverlicense.A) {
                    if (entry instanceof Motobike) {
                        ((Motobike) entry).getOptions();
                    }
                }
                if (getLicense() == Driverlicense.AB) {
                    if (entry instanceof Motobike) ((Motobike) entry).getOptions();
                    else if (entry instanceof Car) ((Car) entry).getOptions();
                }
            }
        }
    }

    @Override
    public void rent(Vehicle vehicle) {
        vehicle.setFree(false);
        if (vehicle instanceof Motobike) {
        ((Motobike) vehicle).rent(vehicle);
        }
        else ((Car) vehicle).getOptions();
    }
}
