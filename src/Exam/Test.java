package Exam;

import java.util.ArrayList;
import java.util.Collections;

public class Test {

    public static void main(String[] args) {

        Driver driver1 = new Driver(Driver.Driverlicense.A);
        Driver driver2 = new Driver(Driver.Driverlicense.AB);
        Driver driver3 = new Driver(Driver.Driverlicense.AB);
        Driver driver4 = new Driver(Driver.Driverlicense.AB);

        Car porsche = new Car(50, "Porsche", "911", true);
        Car audi = new Car(16, "Audi", "A3", true);
        Car gaz = new Car(5, "Gaz", "3310", false);

        Motobike honda = new Motobike(10, "Honda", "CRF450RX", false);
        Motobike voshod = new Motobike(4, "Voshod", "3M", true);
        Motobike minsk = new Motobike(5, "Minsk", "4", true);

        ArrayList<Vehicle> isForRent = new ArrayList<>();
        Collections.addAll(isForRent, porsche, audi, gaz, honda, voshod, minsk);

        System.out.println("парк для аренды");

        for (Vehicle entry: isForRent) {
            entry.getOptions();
        }

        System.out.println("проверка для что доступно водителю и последующая аренда тр. средства");

        driver1.getavailable(isForRent);
        driver1.rent(minsk);
        driver1.getavailable(isForRent);
        driver2.getavailable(isForRent);
        driver2.rent(audi);
        driver3.rent(porsche);
        driver4.getavailable(isForRent);

        minsk.getOptions();
    }
}
