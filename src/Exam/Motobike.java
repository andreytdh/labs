package Exam;

public class Motobike extends Vehicle implements Carsharable {
    private boolean isWithSidecar;

    public Motobike(int pricePermin, String brand, String model, boolean isWithSidecar) {
        super(pricePermin, brand, model);
        this.isWithSidecar = isWithSidecar;
    }

    public boolean getSidecar() {
        return isWithSidecar;
    }





    public void getOptions() {
        System.out.println((super.isFree()? "доступен(на) ":"") +getBrand() + " "+ getModel() +
                (getSidecar()? " c коляской": " без коляски") + " за " + getPricePermin() + "руб. в минуту");
    }

    @Override
    public void rent(Vehicle vehicle) {
        System.out.println(getBrand() + " "+ getModel() + (getSidecar()? " c коляской": " без коляски")
                + " арендован за " + getPricePermin() + "руб. в минуту");
        setFree(false);
    }
}
