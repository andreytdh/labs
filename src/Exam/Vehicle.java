package Exam;

abstract class Vehicle implements Carsharable{
    private int pricePermin;
    private String brand;
    private String model;
    private boolean isFree = true;

    public Vehicle(int pricePermin, String brand, String model) {
        this.pricePermin = pricePermin;
        this.brand = brand;
        this.model = model;
    }

    public int getPricePermin() {
        return pricePermin;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public void getOptions() {
        System.out.println((isFree()? "доступен(на) ":"") +getBrand() + " "+ getModel() + " за " + getPricePermin() + "руб. в минуту");
    }
}
