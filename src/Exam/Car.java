package Exam;


public class Car extends Vehicle implements Carsharable {
    private boolean isAirConditionavailable;

    public Car(int pricePermin, String brand, String model, boolean isAirConditionavailable) {
        super(pricePermin, brand, model);
        this.isAirConditionavailable = isAirConditionavailable;
    }

    public boolean getAircondition() {
        return isAirConditionavailable;
    }

    public void getOptions() {
        System.out.println((super.isFree() ? "доступен(на) " : "") + getBrand() + " " + getModel()
                + (getAircondition() ? " c кондиционером" : " без кондиционера") + " за " + getPricePermin() + "руб. в минуту");
    }

    @Override
    public void rent(Vehicle vehicle) {
        System.out.println(getBrand() + " " + getModel() + (getAircondition() ? " c кондиционером" : " без кондиционера")
                + " арендована за " + getPricePermin() + "руб. в минуту");
        setFree(false);
    }
}
