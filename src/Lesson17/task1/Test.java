package Lesson17.task1;

public class Test {

    public static void main(String[] args) {
        BinaryDoc binaryDoc = new BinaryDoc();
        TextDoc textDoc = new TextDoc();

        textDoc.print();
        textDoc.read();
        textDoc.write();

        binaryDoc.read();
        binaryDoc.run();
        binaryDoc.write();
    }
}
