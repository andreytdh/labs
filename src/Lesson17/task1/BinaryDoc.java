package Lesson17.task1;

import java.util.ArrayList;

public class BinaryDoc extends Document implements Readable, Writable, Runnable {
    byte binary;


    public BinaryDoc() {
        System.out.println("binaryDoc was created: "+ this.creationDate);

    }

    @Override
    public void read() {
        System.out.println("Документ прочитан");
    }

    @Override
    public void write() {
        System.out.println("Данные записаны");
    }

    @Override
    public void run() {
        System.out.println("Файл запущен");
    }
}
