package Lesson17.task1;

public class TextDoc extends Document implements Readable, Writable, Printable {
    String text;
    public TextDoc() {
        System.out.println("textDoc was created: "+ this.creationDate);
    }

    @Override
    public void read() {
        System.out.println("Документ прочитан");
    }

    @Override
    public void write() {
        System.out.println("Данные записаны");
    }

    @Override
    public void print() {
        System.out.println("Документ отправлен на печать");
    }
}
