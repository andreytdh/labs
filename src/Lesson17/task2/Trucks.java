package Lesson17.task2;

public class Trucks extends Cars {
    int volume;
    int loaded;

    public Trucks(int volume, int hp, int weight) {
        this.volume = volume;
        this.loaded = 0;
        super.passengers = 0;
        super.hp =hp;
        super.weight = weight;
        System.out.println("Грузовик создан");
    }

    public void load(int x) {
        if (loaded + x <= this.volume) {
            this.loaded = x;
            System.out.println("Кузов загружен");
        }
        else System.out.println("Груз не помещается в кузов");
    }

    public void unload() {
        if (this.loaded !=0) {
        this.loaded = 0;
            System.out.println("Кузов разгружен");
        }
        else System.out.println("Кузов уже пустой");
    }
}
