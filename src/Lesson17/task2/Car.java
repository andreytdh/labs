package Lesson17.task2;

public class Car extends  Cars{
    int loaded;

    public Car(int weight, int hp, int passengers) {
        super.weight = weight;
        super.hp = hp;
        super.passengers = passengers;
        this.loaded = 0;
        System.out.println("Легковой авто создан");
    }

    public void load(int x) {
        if (loaded + x <= this.passengers) {
            this.loaded = x;
            System.out.println("Пассажиры посажены в автомобиль");
        }
        else System.out.println("Мест уже нет, это не автобус!");
    }

    public void unload() {
        if (this.loaded !=0) {
            this.loaded = 0;
            System.out.println("Пассажиры вышли");
        }
        else System.out.println("В авто только водитель");
    }
}
