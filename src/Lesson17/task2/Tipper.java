package Lesson17.task2;

public class Tipper extends Trucks{
    boolean isUp;
    int loaded;

    public Tipper(int volume, int hp, int weight) {
        super(volume, hp, weight);
        this.isUp = false;
        this.loaded = 0;
    }

    public void upVolume() {
        if (loaded!=0) {
            this.isUp = true;
            System.out.println("Кузов поднят");
            super.unload();
        }

        else System.out.println("Пустой кузов поднят");
    }

    public void downVolume() {
        this.isUp = false;
        this.loaded = 0;
        System.out.println("Кузов опущен, можно загружать");
    }
}
