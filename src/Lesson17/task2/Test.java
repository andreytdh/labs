package Lesson17.task2;

public class Test {

    public static void main(String[] args) {
        Tipper tipper = new Tipper(1000, 400, 6000);
        tipper.downVolume();

        Car volvo = new Car(2000, 230, 4);
        volvo.load(2);

        Trucks truck = new Trucks(500, 250, 3500);
        truck.load(501);
    }
}
