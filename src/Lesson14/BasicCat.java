package Lesson14;

import java.util.ArrayList;

public class BasicCat {
    private String name;
    private double age;
    private int weight;
    protected static int numKittens;
    protected static ArrayList<Object> basicCats = new ArrayList<>();


    public BasicCat(String name, double age) {
        this.name = name;
        this.age = age;
        if (age < 1) {
            numKittens++;
            this.weight = 1;
        }
        else {
            this.weight = 3;
        }
        basicCats.add(this);
    }

    public double getAge() {
        return age;
    }

    public static void getNumberOfCats() {
        System.out.println("Всего создано: " + basicCats.size() + " котов и кошек");
    }
    public static void getNumbersOfKitten() {
        System.out.println("Всего создано: " + numKittens + " котят");
    }

}
