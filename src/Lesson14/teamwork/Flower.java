package Lesson14.teamwork;

/*
1. Создать класс Flower, который содержит переменные страна-производитель,
 срок хранения в днях, цена. Создать классы расширяющие класс Flower
 (для примера, пусть это будут розы, гвоздики, тюльпаны и... что-то на свой вкус).
  Пусть каждый из этих цветов можно будет взять или положить на место.
  Собрать 3 букета с определением их стоимости. В букет может входить несколько цветов разного типа.
   Также подсчитать количество проданных цветов.

rose, tulip, carnation, chrysanthemum
 */
public class Flower {
    private String manufacturerCountry;
    private int validity;
    private double price;


    static int flowerscount = 0;

    public Flower(String manufacturerCountry, int validity, double price) {
        this.manufacturerCountry = manufacturerCountry;
        this.validity = validity;
        this.price = price;
    }

    public String getManufacturerCountry() {
        return manufacturerCountry;
    }

    public void setManufacturerCountry(String manufacturerCountry) {
        this.manufacturerCountry = manufacturerCountry;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity) {
        this.validity = validity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}
