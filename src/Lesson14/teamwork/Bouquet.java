package Lesson14.teamwork;

import java.util.ArrayList;

public class Bouquet {
    private int roseCounter;
    private int tulipCounter;
    private int carnationCounter;
    private int chrysanthemumCounter;
    private ArrayList<Flower> bouquet;

    public Bouquet(int roseCounter, int tulipCounter, int carnationCounter, int chrysanthemumCounter) {
        this.roseCounter = roseCounter;
        this.tulipCounter = tulipCounter;
        this.carnationCounter = carnationCounter;
        this.chrysanthemumCounter = chrysanthemumCounter;

        bouquet = new ArrayList<Flower>();
        if (roseCounter > 0) {
            for (int i = 0; i < roseCounter; i++) {
                bouquet.add(new Rose("Russia", 8, 98.5));
                Flower.flowerscount++;
            }
        }
        if (tulipCounter >0) {
            for (int i = 0; i < tulipCounter; i++) {
                bouquet.add(new Tulip("Netherlands", 10, 50.0));
                Flower.flowerscount++;
            }
        }
        if (carnationCounter > 0) {
            for (int i = 0; i < carnationCounter; i++) {
                bouquet.add(new Carnation("Russia", 5, 60.5));
                Flower.flowerscount++;
            }
        }
        if (chrysanthemumCounter > 0) {
            for (int i = 0; i < chrysanthemumCounter; i++) {
                bouquet.add(new Chrysanthemum("Japan", 15, 100));
                Flower.flowerscount++;
            }
        }

    }

    public static void getPriceofBouquet(Bouquet bouquet) {
        double price=0;
        for (Flower object: bouquet.bouquet) {
            price += object.getPrice();
        }
        System.out.println("price of the bouquet is: " + price + " RUB");
    }

    public static void getAmountofFlowers() {
        System.out.println("Totally " + Flower.flowerscount + " flowers were sold.");
    }

    public static void addFlower(Bouquet bouquet, Flower flower) {
                    bouquet.bouquet.add(flower);
                    Flower.flowerscount++;
    }

}
