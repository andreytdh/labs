package Lesson14.teamwork;

public class Test {

    public static void main(String[] args) {

        Bouquet bouquet1 = new Bouquet(10, 0, 0, 0);
        Bouquet.getPriceofBouquet(bouquet1);
        Bouquet.getAmountofFlowers();

        Bouquet.addFlower(bouquet1, new Rose("Russia", 10, 100));

        Bouquet.getPriceofBouquet(bouquet1);
        Bouquet.getAmountofFlowers();
    }
}
