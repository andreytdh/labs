package Lesson15.task2;

public interface Cookable {
    public void cook() throws InterruptedException;
}
