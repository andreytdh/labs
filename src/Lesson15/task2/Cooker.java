package Lesson15.task2;

import sun.rmi.server.InactiveGroupException;

public  class Cooker implements Cookable {
    private Integer temp;
    private Boolean vkl;
    private Boolean load;
    private Boolean elkt;
    //private int temperature;
    //private boolean isEnabled;
    //private boolean isLoaded;
    //private boolean isElektro;
    public Integer mintemp = Integer.valueOf(0);
    public Integer maxtemp = Integer.valueOf(220);

    public Cooker(boolean isElektro) {
        this.temp = new Integer(0);
        this.vkl = false;
        this.load = false;
        this.elkt = isElektro;
    }

    public Integer getTemperature() {
        return temp;
    }

    public void setTemperature(int temperature) {
        if (temperature < mintemp) {
            System.out.println("Operation is not supported. It's not a fridge!");
        }
        else if (temperature == mintemp) {
            this.temp = new Integer(temperature);
            this.vkl = false;
        }
        else if (temperature > mintemp && temperature <=maxtemp) {
            this.temp = new Integer(temperature);
            this.vkl = true;
        }
        else if (temperature > maxtemp) {
            System.out.println("Operation is not supported.");
        }
    }

    public Boolean isEnabled() {
        return vkl;
    }

    public void setEnabled(boolean enabled) {
        vkl = enabled;
        if (vkl = false) {
            this.setTemperature(0);
        }
    }

    public Boolean isLoaded() {
        return load;
    }

    public void setLoaded(boolean loaded) {
        this.load = loaded;
    }

    @Override
    public void cook() throws InterruptedException {
        setLoaded(true);
        setEnabled(true);
        setTemperature(180);
        System.out.println("Please wait for your dish");
        Thread.sleep(2000);
        System.out.println("Your dish is ready. Take it off");
        setEnabled(false);
    }
}
