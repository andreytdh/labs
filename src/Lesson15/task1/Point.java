package Lesson15.task1;

public class Point {
    private Integer iks;
    private Integer igrik;
    //private int x;
    //private int y;

    public Point(int x, int y) {
        this.iks = new Integer(x);
        this.igrik = new Integer(y);
    }

    public int getX() {
        return iks;
    }

    public void setX(int x) {
        this.iks = x;
    }

    public int getY() {
        return igrik;
    }

    public void setY(int y) {
        this.igrik = y;
    }
}
