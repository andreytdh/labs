package Lesson15.task1;

public class Circle implements Movable {
    private Integer rad;
    //private int radius;
    private Point center;

    public Circle(int radius) {
        this.rad = new Integer(radius);
        this.center = new Point(0,0);
        System.out.println("Circle with center (" + this.center.getX() + "; " + this.center.getY() + ") was created");
    }

    public static void getPerimeter(Circle circle) {
        System.out.println("Perimeter is: " + Double.valueOf((double) circle.rad*2*Math.PI));
    }

    public static void getArea(Circle circle) {
        System.out.println("Area is: " + Double.valueOf((double) circle.rad*circle.rad*Math.PI));
    }


    @Override
    public void moveTo(int x, int y) {
        System.out.println("Current position is: x = "+ this.center.getX() + "; y = " + this.center.getY());
        this.center.setX(x);
        this.center.setY(y);
        System.out.println("Position changed to: x = "+ this.center.getX() + "; y = " + this.center.getY());
    }
}
