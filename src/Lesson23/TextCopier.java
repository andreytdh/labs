package Lesson23;

import java.io.*;

public class TextCopier {

    public static String readFile(File filename) {
        String content = "";
        String text = "";
        try (FileReader fileReader = new FileReader(filename)) {
            BufferedReader br = new BufferedReader(fileReader);
            while ((text = br.readLine()) != null) {
                content += text + "\n";
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static void copyTextTo(String filename, String s) {
        File output = new File(filename);
        try {
            output.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (output.exists() & output.canWrite()) {
            try (FileWriter fileWriter = new FileWriter(output)) {
                BufferedWriter bw = new BufferedWriter(fileWriter);
                bw.write(s);
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        File input = new File("input.txt");
        if (input.exists() & input.canRead()) {
            copyTextTo("output.txt", readFile(input));
        }
    }
}
