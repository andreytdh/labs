package Lesson23;

import java.io.File;

public class DirectoryList {

    public static void getList(File dirname) {
        if (dirname.isDirectory()) {
            for (File entry : dirname.listFiles()) {
                if (entry.isDirectory()) {
                    System.out.println("d: " + entry.getName());
                    getList(entry);
                } else System.out.println(entry.getName());
            }
        }
    }

    public static void main(String[] args) {
        File dir = new File("C:\\Users\\anvasile\\Documents\\Java_automation\\Labs");
        getList(dir);
    }
}
