package Lesson23;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriterToOUT {

    public static void main(String[] args) {
        File out = new File("out.txt");
        String input = "\"Это\"\n\"Файл\"\n\"Записанный\"\n\"Программой на JAVA!\"\n";
        String task3 = "\"Написанной студентом Васильевым Андреем\"";
        FileWriter fw = null;
        if (out.exists()) {
            try {
                fw = new FileWriter(out, true);
                fw.write(input);
                fw.write(task3);
                fw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
