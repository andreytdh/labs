package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab5_2 {

    public static boolean isTrue(int a, int b, int c) {
        if (a < b && b < c) {
            return true;
        }
        else return false;
    }

    public static void main(String[] args) throws IOException {
        int a, b, c;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        a = Integer.parseInt(br.readLine());
        b = Integer.parseInt(br.readLine());
        c = Integer.parseInt(br.readLine());

        System.out.println(isTrue(a, b, c));
    }
}
