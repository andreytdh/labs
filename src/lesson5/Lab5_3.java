package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab5_3 {

    public static int factorial(int x) {
        int result =1;
        for (int i = 1; i <= x; i++) {
            result *= i;
        }
        return result;
    }
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(br.readLine());
        if (a > 1) {
            System.out.println(factorial(a));;
        }
    }
}
