package lesson5;

public class Lab5_4 {
    static int x = 1234567;
    static int  result = 0;
    public static int reverseDigit(int a) {
        while (a != 0) {
            int x = a %10;
            result = result* 10 + x;
            a /= 10;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(reverseDigit(x));
    }
}
