package lesson5;

//Найти среднее арифметическое суммы 10 первых чисел, кратных трем, начиная с 1 (1 + 3 + 9 + 27 + …, + 3^9) / 10

public class Lab5_5 {
    static int digit = 3;
    static int sum = 0;
    public static void main(String[] args) {
        for (int i = 0; i < 10 ; i++) {
            sum += (int) Math.pow(3 , i);
        }

        System.out.println((double) sum/10);
    }
}
