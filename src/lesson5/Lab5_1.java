package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab5_1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(br.readLine());

        switch (x) {
            case 0:
                System.out.println("зима");
                break;
            case 1:
                System.out.println("весна");
                break;
            case 2:
                System.out.println("лето");
                break;
            case 3:
                System.out.println("осень");
                break;
            default:
                System.out.println("такого времени года нет");
        }
    }
}
