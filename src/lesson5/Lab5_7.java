package lesson5;

//Проверить, является ли число степенью двойки. Вывести «да» или «нет». По желанию вывести степень.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab5_7 {

    public static boolean isTrue(int x) {
        if (x == 0) {
            return false;
        }
        while (x != 1) {
            if (x % 2 != 0)
                return  false;
                x = x/2;
        }
        return true;
    }

    public static int result(int x) {
        int result = 0;
        while (x > 1) {
            if (x % 2 == 0) {
                result ++;
                x = x/2;
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(br.readLine());

        if (isTrue(x)) {
            System.out.println("yes");
            System.out.println(result(x));
        }
        else System.out.println("no");

    }
}
