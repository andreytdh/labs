package lesson5;


//Найти количество цифр заданного числа n типа long (например, 1000 – 4 цифры)
public class Lab5_6 {

    public static void main(String[] args) {
        long test = 12535094;
        int count = 0;

        while (test != 0) {
            count++;
            test/= 10;
        }
        System.out.println(count);
    }

}
