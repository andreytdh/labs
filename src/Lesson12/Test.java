package Lesson12;

public class Test {

    public static void main(String[] args) {
        Address address1 = new Address("Russia", "Saint-Petersburg", "13 Liniya V.O.", 14);
        Address address2 = new Address("Germany");
        Address addresshome1 = new Address("Russia", "Saint-Petersburg");
        Address addresshome2 = new Address("Russia", "Magadan");
        Address addresshome3 = new Address("Germany", "Potsdam");

        Person me = new Person("Andrei", 32, addresshome1, address1);
        Person Petya = new Person("Petya", 29, addresshome2, address1);
        Person Sven = new Person("Sven", 41, addresshome3, address2);


        Address.printAddress(me.getWorkAddress());
        Address.printAddress(Petya.getWorkAddress());
        Address.printAddress(Sven.getWorkAddress());


    }
}
