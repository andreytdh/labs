package Lesson12;

public class Person {
    public String name;
    private int age;
    private Address homeAddress;
    private Address workAddress;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, Address homeAddress) {
        this(name, age);
        this.homeAddress = homeAddress;
    }

    public Person(String name, Address workAddress, int age) {
        this(name, age);
        this.workAddress = workAddress;
    }

    public Person(String name, int age, Address homeAddress, Address workAddress) {
        this(name, workAddress, age);
        this.homeAddress = homeAddress;
    }



    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private Address getHomeAddress() {
        return homeAddress;
    }

    private void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }


    public static void printHome(Person person) {
        System.out.println(person.workAddress.country + ", "
                + (person.workAddress.getCity().isEmpty() ?"":person.workAddress.getCity()) + ", "
                + (person.workAddress.getStreet().isEmpty() ?"":person.workAddress.getStreet()) + ", "
                + (person.workAddress.getHousenumber()==0 ? "":person.workAddress.getHousenumber()));
    }


}
