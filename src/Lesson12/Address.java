package Lesson12;

public class Address {

        public String country;
        private String city;
        private String street;
        private int housenumber;
        private int index;

        public Address(String country) {
                this.country = country;
                this.housenumber=0;
        }

        public Address(String country, String city) {
                this(country);
                this.city=city;
        }


        public Address(String country, String city, String street, int housenumber) {
                this.country = country;
                this.city = city;
                this.street = street;
                this.housenumber = housenumber;
        }

        public Address(String country, String city, String street, int housenumber, int index) {
                this(country, city, street, housenumber);
                this.index = index;
        }

        public static void printAddress(Address address){
                System.out.println(((address.country!=null)?"Country: "+ address.country:" ")
                + ((address.city!=null)?", City: " + address.city:" ") + ((address.street!=null)?", Street: " +address.street:" ")
                    + ((address.housenumber != 0)?", Housenumber: " + address.housenumber:" ")
                        +  ((address.index != 0)?", Index: " +address.index:" "));

        }

        public String getCity() {
                return city;
        }

        public String getStreet() {
                return street;
        }

        public int getHousenumber() {
                return housenumber;
        }

        public int getIndex() {
                return index;
        }
}

